import * as globals from "../init/globals";
import {Container} from "pixi.js";
import {CenteredSprite} from "../utils/CenteredSprite";
import * as textures from "../init/textures";
import * as layers from "../init/layers";

interface GrailModel {
    x: number,
    y: number,
    helpX: number,
    helpY: number,
}

function createGrail() {
    const a = Math.random() * Math.PI * 2;
    const helpA = a + (Math.random() - 0.5);
    const r = globals.worldSide / 2 * 0.7;
    return {
        x: globals.playerStartX + Math.round(r * Math.sin(a)),
        y: globals.playerStartY + Math.round(r * Math.cos(a)),
        helpX: globals.playerStartX + Math.round(r * 0.4 * Math.sin(helpA)),
        helpY: globals.playerStartY + Math.round(r * 0.4 * Math.cos(helpA)),
    }
}

class GrailView extends Container {
    constructor(
        public model: GrailModel,
    ) {
        super();
        const tile = new CenteredSprite(textures.grail);
        this.addChild(tile);
        tile.position.set(this.model.x * globals.tileSide, this.model.y * globals.tileSide);

        const helpTile = new CenteredSprite(textures.grailHelp);
        this.addChild(helpTile);
        helpTile.position.set(this.model.helpX * globals.tileSide, this.model.helpY * globals.tileSide);

        layers.grail.addChild(this);
    }
}

export class Grail {
    model: GrailModel;
    view: GrailView;

    constructor() {
        this.model = createGrail();
        this.view = new GrailView(this.model);
    }
}
