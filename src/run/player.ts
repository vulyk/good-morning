import {Text} from "pixi.js";
import * as layers from "../init/layers";
import * as globals from "../init/globals";
import {comparePatterns, getProjection} from "./pattern";
import {gameOverGui, grail, pattern} from "./modules";

export enum Direction {
    North,
    South,
    East,
    West,
}

interface PlayerModel {
    x: number,
    y: number,
    direction: Direction,
    discomfort: number,
    discomfortCap: number,
    expectedRest: number,
    day: number,
}

function getSymForDirection(direction: Direction) {
    switch (direction) {
        case Direction.West: {
            return "⮜";
        }
        case Direction.East: {
            return "⮞";
        }
        case Direction.North: {
            return "⮝";
        }
        case Direction.South: {
            return "⮟";
        }
    }
}

class PlayerView extends Text {
    constructor(
        public model: PlayerModel,
    ) {
        super();
        this.style.fill = globals.palette.attention;
        this.style.fontSize = globals.tileSide * 2;
        this.style.fontFamily = "Courier New";
        this.style.align = "center";
        this.style.strokeThickness = 4;
        this.style.stroke = globals.palette.back;
        this.anchor.set(0.5);

        layers.player.addChild(this);
    }

    updateTransform() {
        this.text = getSymForDirection(this.model.direction);
        layers.world.position.set(- this.model.x * globals.tileSide, - this.model.y * globals.tileSide);
        layers.grail.position.set(- this.model.x * globals.tileSide, - this.model.y * globals.tileSide);


        super.updateTransform();
    }
}



export class Player {
    model: PlayerModel;
    view: PlayerView;

    constructor() {
        this.model =  {
            x: globals.playerStartX,
            y: globals.playerStartY,
            direction: Direction.South,
            discomfort: 0,
            discomfortCap: 40,
            expectedRest: 0,
            day: 0,
        };
        this.updateExpectedRest();
        this.view = new PlayerView(this.model);

        window.addEventListener("keydown", e => {
            if (gameOverGui.gameOver) {
                return;
            }

            switch (e.keyCode)  {
                case 65: {
                    this.move(Direction.West);
                    break;
                }
                case 68: {
                    this.move(Direction.East);
                    break;
                }
                case 87: {
                    this.move(Direction.North);
                    break;
                }
                case 83: {
                    this.move(Direction.South);
                    break;
                }
                case 32: {
                    this.sleep();
                    pattern.addSlice();
                    this.updateExpectedRest();
                    break;
                }
            }
        });
    }

    updateExpectedRest() {
        this.model.expectedRest = Math.floor(getProjection(pattern.model, this.model.x, this.model.y));
    }

    move(direction: Direction) {
        switch (direction) {
            case Direction.West: {
                this.model.x--;
                break;
            }
            case Direction.East: {
                this.model.x++;
                break;
            }
            case Direction.North: {
                this.model.y--;
                break;
            }
            case Direction.South: {
                this.model.y++;
                break;
            }
        }

        this.model.direction = direction;
        this.model.discomfort++;
        this.updateExpectedRest();
        if (this.model.discomfort > this.model.discomfortCap) {
            gameOverGui.lose();
        }
        if (this.model.x === grail.model.x && this.model.y === grail.model.y) {
            gameOverGui.win();
        }
    }

    sleep() {
        this.model.discomfort = Math.max(0,
            this.model.discomfort - this.model.expectedRest);
        this.model.day++;
    }
}
