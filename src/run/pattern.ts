import {player, world} from "./modules";
import {Container, Sprite} from "pixi.js";
import {CenteredSprite} from "../utils/CenteredSprite";
import {tileSide} from "../init/globals";
import * as textures from "../init/textures";
import * as globals from "../init/globals";
import * as layers from "../init/layers";



type PatternSlice = Array<Array<string>>;
type PatternModel = {
    patternHistory: PatternSlice[],
};


function copyPattern(cx: number, cy: number) {
    const fixedPattern: PatternSlice = Array.from(
        {length: globals.patternSide},
        () => Array.from({length: globals.patternSide}));

    for (let x = -globals.patternHalfSide; x <= globals.patternHalfSide; x++) {
        for (let y = -globals.patternHalfSide; y <= globals.patternHalfSide; y++) {
            const wx = cx + x;
            const wy = cy + y;

            if (wx < 0 || wx >= globals.worldSide || wy < 0 || wy >= globals.worldSide) {
                continue;
            }

            fixedPattern[globals.patternHalfSide + x][globals.patternHalfSide + y]
                = world.model[wx][wy];
        }
    }

    return fixedPattern;
}

function compareSyms(s1: string, s2: string) {
    if (s1 === s2) {
        return 1;
    } else {
        return 0;
    }
}

export function comparePatterns(pattern: PatternSlice, cx: number, cy: number) {
    let sum = 0;
    for (let x = -globals.patternHalfSide; x <= globals.patternHalfSide; x++) {
        for (let y = -globals.patternHalfSide; y <= globals.patternHalfSide; y++) {
            const wx = cx + x;
            const wy = cy + y;

            if (wx < 0 || wx >= globals.worldSide || wy < 0 || wy >= globals.worldSide) {
                continue;
            }

            sum += compareSyms(
                pattern[globals.patternHalfSide + x][globals.patternHalfSide + y],
                world.model[wx][wy]
            ) * Math.min(1, 4 * Math.exp(-(x * x + y * y) / 4));
        }
    }
    return sum;
}

export function getProjection(model: PatternModel, cx: number, cy: number) {
    let sum = 0;
    for (let i = 0; i < globals.patternHistoryDepth; i++) {
        sum += globals.patternHistoryWeights[i] * comparePatterns(model.patternHistory[i], cx, cy);
    }
    return sum;
}

class FixedPatternView extends Container {
    tiles: Array<Array<Sprite>> = Array.from(
        {length: globals.patternSide},
        () => Array.from({length: globals.patternSide}));

    constructor(
        public model: PatternModel
    ) {
        super();

        for (let x = 0; x < globals.patternSide; x++) {
            for (let y = 0; y < globals.patternSide; y++) {
                const tile = this.tiles[x][y] = new CenteredSprite();
                tile.tint = 0xf00000;
                const _x = x - globals.patternHalfSide;
                const _y = y - globals.patternHalfSide;
                tile.alpha = 0.8 * Math.min(1, 4 * Math.exp(-(_x * _x + _y * _y) / 4));
                this.addChild(tile);
                tile.position.set(x * tileSide, y * tileSide);
            }
        }

        this.position.set(- globals.patternHalfSide * tileSide, - globals.patternHalfSide * tileSide);
        this.visible = false;

        layers.fixedPattern.addChild(this);
    }

    updateTransform() {
        for (let x = 0; x < globals.patternSide; x++) {
            for (let y = 0; y < globals.patternSide; y++) {
                const tile = this.tiles[x][y];
                const w: { [sym: string]: number } = {};
                let maxW = 0;
                let maxSym = globals.syms[0];
                for (let i = 0; i < globals.patternHistoryDepth; i++) {
                    const sym = this.model.patternHistory[i][x][y];
                    if ("undefined" === typeof w[sym]) {
                        w[sym] = 0;
                    }
                    w[sym] += globals.patternHistoryWeights[i];
                    if (w[sym] > maxW) {
                        maxW = w[sym];
                        maxSym = sym;
                    }
                }

                tile.texture = textures.syms.get(maxSym)!;
            }
        }

        super.updateTransform();
    }
}

export class Pattern {
    model: PatternModel;
    view: FixedPatternView;

    constructor() {
        const patternSlice = copyPattern(globals.playerStartX, globals.playerStartY);

        this.model = {
            patternHistory: Array.from(
                {length: globals.patternHistoryDepth},
                () => patternSlice),
        };
        this.view = new FixedPatternView(this.model);

        window.addEventListener("keydown", e => {
            switch (e.keyCode)  {
                case 67: {
                    this.addSlice();
                    break;
                }
                case 86: {
                    this.view.visible = !this.view.visible;
                    break;
                }
            }
        });
    }

    addSlice() {
        this.model.patternHistory.pop();
        this.model.patternHistory.unshift(copyPattern(player.model.x, player.model.y));
    }
}
