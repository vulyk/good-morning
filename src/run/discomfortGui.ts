import {Container, Text} from "pixi.js";
import * as layers from "../init/layers";
import {player} from "./modules";
import * as globals from "../init/globals";


class DiscomfortView extends Container {
    text: Text;

    constructor() {
        super();

        layers.ui.addChild(this);

        this.text = new Text();
        this.text.style.fill = globals.palette.text;
        this.text.style.fontSize = 70;
        this.text.style.strokeThickness = 8;
        this.text.style.stroke = globals.palette.attention;

        this.addChild(this.text);

        this.position.set(- globals.width / 2 + 30, - globals.height / 2 + 30);
    }

    updateTransform() {
        this.text.text = ` (>_<)   ${player.model.discomfort} (-${player.model.expectedRest})`
            + ` / ${player.model.discomfortCap}`;

        super.updateTransform();
    }
}

export class DiscomfortGui {
    view: DiscomfortView;

    constructor() {
        this.view = new DiscomfortView();
    }
}
