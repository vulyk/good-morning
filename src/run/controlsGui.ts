import {Container, Text} from "pixi.js";
import * as layers from "../init/layers";
import {player} from "./modules";
import * as globals from "../init/globals";


class ControlsView extends Container {
    text: Text;

    constructor() {
        super();

        layers.ui.addChild(this);

        this.text = new Text();
        this.text.text = " |W|\n|A||S||D| |____|\n";
        this.text.style.fill = globals.palette.text;
        this.text.style.fontFamily = "Courier New";
        this.text.style.fontSize = 50;
        this.text.style.strokeThickness = 8;
        this.text.style.stroke = globals.palette.attention;
        this.text.anchor.set(0, 1);

        this.addChild(this.text);

        this.position.set(
            - globals.width / 2 + 30,
            + globals.height / 2 + 30);
    }

    updateTransform() {
        super.updateTransform();
    }
}

export class ControlsGui {
    view: ControlsView;

    constructor() {
        this.view = new ControlsView();
    }
}
