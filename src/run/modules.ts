import {World} from "./world";
import {Player} from "./player";
import {Pattern} from "./pattern";
import {DiscomfortGui} from "./discomfortGui";
import {DayGui} from "./dayGui";
import {Grail} from "./grail";
import {GameOverGui} from "./gameOverGui";
import {ControlsGui} from "./controlsGui";

export const world = new World();
export const grail = new Grail();
export const pattern = new Pattern();
export const player = new Player();
export const discomfortGui = new DiscomfortGui();
export const dayGui = new DayGui();
export const gameOverGui = new GameOverGui();
export const controlsGui = new ControlsGui();
