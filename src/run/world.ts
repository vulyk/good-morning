import {tileSide, worldSide} from "../init/globals";
import {getRandomElement} from "../utils/misc";
import * as globals from "../init/globals";
import {Container} from "pixi.js";
import {CenteredSprite} from "../utils/CenteredSprite";
import * as textures from "../init/textures";
import * as layers from "../init/layers";

type WorldModel = Array<Array<string>>;

function generateWorld() {
    const world: WorldModel = Array.from(
        {length: worldSide},
        () => Array.from({length: worldSide}, () => globals.syms[0]));

    for (let i = 0; i < 20; i++) {
        const patchX = Math.floor(Math.random() * worldSide);
        const patchY = Math.floor(Math.random() * worldSide);
        const patchR = 20 + Math.round(Math.random() * 10);
        const sym = getRandomElement(globals.syms);

        for (let dx = -patchR; dx <= patchR; dx++) {
            for (let dy = -patchR; dy <= patchR; dy++) {
                if (dx * dx + dy * dy > patchR * patchR) {
                    continue;
                }
                const x = patchX + dx;
                const y = patchY + dy;
                if (x < 0 || x >= worldSide || y < 0 || y >= worldSide) {
                    continue;
                }
                world[x][y] = sym;
            }
        }
    }
    for (let i = 0; i < 1000; i++) {
        const patchX = Math.floor(Math.random() * worldSide);
        const patchY = Math.floor(Math.random() * worldSide);
        const patchR = 1 + Math.round(Math.random() * 2);
        const sym = getRandomElement(globals.syms);

        for (let dx = -patchR; dx <= patchR; dx++) {
            for (let dy = -patchR; dy <= patchR; dy++) {
                if (dx * dx + dy * dy > patchR * patchR) {
                    continue;
                }
                const x = patchX + dx;
                const y = patchY + dy;
                if (x < 0 || x >= worldSide || y < 0 || y >= worldSide) {
                    continue;
                }
                world[x][y] = sym;
            }
        }
    }
    for (let i = 0; i < 3000; i++) {
        const patchX = Math.floor(Math.random() * worldSide);
        const patchY = Math.floor(Math.random() * worldSide);
        const patchR = 0;
        const sym = getRandomElement(globals.syms);

        for (let dx = -patchR; dx <= patchR; dx++) {
            for (let dy = -patchR; dy <= patchR; dy++) {
                if (dx * dx + dy * dy > patchR * patchR) {
                    continue;
                }
                const x = patchX + dx;
                const y = patchY + dy;
                if (x < 0 || x >= worldSide || y < 0 || y >= worldSide) {
                    continue;
                }
                world[x][y] = sym;
            }
        }
    }

    return world;
}

class WorldView extends Container {
    constructor(
        public model: WorldModel,
    ) {
        super();
        for (let x = 0; x < this.model.length; x++) {
            for (let y = 0; y < this.model[x].length; y++) {
                const tile = new CenteredSprite(textures.syms.get(this.model[x][y]));
                tile.tint = globals.palette.main;
                this.addChild(tile);
                tile.position.set(x * tileSide, y * tileSide);
            }
        }
        layers.world.addChild(this);
    }
}

export class World {
    model: WorldModel;
    view: WorldView;

    constructor() {
        this.model = generateWorld();
        this.view = new WorldView(this.model);
    }
}
