import {Container, Text} from "pixi.js";
import * as layers from "../init/layers";
import {player} from "./modules";
import * as globals from "../init/globals";


class GameOverView extends Container {
    text: Text;
    starText: Text;

    constructor() {
        super();

        layers.ui.addChild(this);

        this.text = new Text();
        this.text.style.fill = globals.palette.text;
        this.text.style.strokeThickness = 25;
        this.text.style.stroke = globals.palette.attention;
        this.text.anchor.set(0.5);

        this.starText = new Text();
        this.starText.style.fill = globals.palette.attention;
        this.starText.style.fontSize = 450;
        this.starText.style.strokeThickness = 30;
        this.starText.style.stroke = globals.palette.text;
        this.starText.anchor.set(0.5);

        this.addChild(this.text);
        this.addChild(this.starText);
        this.visible = false;
    }

    win(stars: number) {
        this.text.text = `＼( ^⋅^ )／`;
        this.text.style.fontSize = 150;
        this.text.position.set(0, -150);
        let starsText = "";
        for (let i = 0; i < 5; i++) {
            starsText += i < stars ? "⭒" : "⭑";
        }
        this.starText.text = starsText;
        this.starText.position.set(0, 150);
        this.visible = true;
    }

    lose() {
        this.text.text = `☠`;
        this.text.style.fontSize = 550;
        this.text.position.set(0, 0);
        this.starText.text = "";
        this.visible = true;
    }
}

export class GameOverGui {
    view: GameOverView;

    gameOver = false;

    constructor() {
        this.view = new GameOverView();
    }

    win() {
        if (this.gameOver) {
            return;
        }
        this.gameOver = true;


        this.view.win((() => {
            if (player.model.day < 5) {
                return 5;
            } else if (player.model.day < 10) {
                return 4;
            } else if (player.model.day < 20) {
                return 3;
            } else if (player.model.day < 50) {
                return 2;
            } else if (player.model.day < 100) {
                return 1;
            } else {
                return 0;
            }
        })());
    }

    lose() {
        if (this.gameOver) {
            return;
        }
        this.gameOver = true;

        this.view.lose();
    }
}
