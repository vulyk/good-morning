import {Graphics, Text, Texture} from "pixi.js";
import {app} from "./app";
import * as globals from "./globals";

function renderSym(sym: string) {
    const t = new Text();
    t.text = sym;
    t.style.fill = "white";
    t.style.fontSize = globals.tileSide;
    t.style.fontFamily = "Courier New";
    return app.renderer.generateTexture(t);
}

export const syms = new Map(
    [...globals.syms]
        .map(sym => [sym, renderSym(sym)] as [string, Texture]));

export const grail = (() => {
    const t = new Text();
    t.text = "⚿";
    t.style.fontSize = globals.tileSide * 2;
    t.style.fontFamily = "Courier New";
    t.style.fill = globals.palette.attention;
    t.style.strokeThickness = 4;
    t.style.stroke = globals.palette.back;
    return app.renderer.generateTexture(t);
})();

export const grailHelp = (() => {
    const t = new Text();
    t.text = "⭒";
    t.style.fontSize = globals.tileSide * 3;
    t.style.fontFamily = "Courier New";
    t.style.fill = globals.palette.attention;
    t.style.strokeThickness = 3;
    t.style.stroke = globals.palette.back;
    return app.renderer.generateTexture(t);
})();
