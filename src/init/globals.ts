export const width = 1960;
export const height = 1080;

export const syms = ["⋅", "🏵", "▓", "☘", "⸙"];

export const worldSide = 150;

export const playerStartX = Math.floor(worldSide / 2);
export const playerStartY = Math.floor(worldSide / 2);

export const tileSide = 35;

export const patternHalfSide = 7;
export const patternSide = patternHalfSide * 2 + 1;
export const patternHistoryWeight = 0.9;
export const patternHistoryWeights = [
    (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 0),
    (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 1),
    (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 2),
    (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 3),
    (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 4),
    (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 5),
    (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 6),
    (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 7),
    (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 8),
    (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 9),
];
export const patternHistoryDepth = patternHistoryWeights.length;

export const palette = {
    back: 0xd0d0c0,
    main: 0x5050a0,
    attention: 0x101060,
    text: 0xd0d0d0,
};
