import {app} from "./app";
import {Container} from "pixi.js";

export const background = app.stage.addChild(new Container());
export const world = app.stage.addChild(new Container());
export const grail = app.stage.addChild(new Container());
export const fixedPattern = app.stage.addChild(new Container());
export const player = app.stage.addChild(new Container());
export const ui = app.stage.addChild(new Container());
export const win = app.stage.addChild(new Container());
