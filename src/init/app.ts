import {tap} from "../utils/misc";
import {Application} from "pixi.js";
import * as globals from "./globals";

export const canvas = document.getElementById("canvas")! as HTMLCanvasElement;

export const app = tap(new Application({
    view: canvas,
    width: globals.width,
    height: globals.height,
    backgroundColor: globals.palette.back,
}), app => {
    app.loader.baseUrl = "./assets/";
}, app => {
    app.stage.position.set(app.renderer.width / 2, app.renderer.height / 2);
});
