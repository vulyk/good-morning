export function isVisible(elt: Element): boolean {
    const style = window.getComputedStyle(elt);
    return (style.width !== null && +style.width !== 0)
        && (style.height !== null && +style.height !== 0)
        && (style.opacity !== null && +style.opacity !== 0)
        && style.display !== "none"
        && style.visibility !== "hidden";
}

export function tap<T>(
    x: T,
    ...fns: Array<((x: T) => void)>
): T {
    for (const fn of fns) {
        fn(x);
    }
    return x;
}

export function getRandomElement<T>(array: T[], getRandomFloat = Math.random): T {
    return array[Math.floor(getRandomFloat() * array.length)];
}

export type Nullable<T> = T | null;
