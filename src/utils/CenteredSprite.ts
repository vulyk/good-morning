import * as PIXI from "pixi.js";
import Texture = PIXI.Texture;
import Sprite = PIXI.Sprite;

export class CenteredSprite extends Sprite {
    constructor(texture?: Texture) {
        super(texture);
        this.anchor.set(0.5);
    }
}
