import {app} from "./init/app";

app.loader.load(() => {
    // noinspection JSIgnoredPromiseFromCall
    SystemJS.import("./run/run");
});
