# Welcome to *Good Morning!* game!

[Click to play *Good Morning!*](https://ndry.bitbucket.io/good-morning/) – 
the game about leaving the comfort zone and making yourself at home wherever you go.

Your goal is to find a key.

While wandering around, the hero has to sleep for recreation.
Still, the key has to be found through the fewest nightstays possible.
Every journey step increases your stress level, 
which causes death once reaching the limit.
Your character recreates best sleeping in familiar places, reminding him home.
The home is where the player starts at the beginning,
but the memory shifts with further nightstays.
So, the ideal sleeping strategy is not to change the sleeping site,
but as journey calls, the hero leaves the comfort zone and find the precious key!

[Click to play *Good Morning!*](https://ndry.bitbucket.io/good-morning/)
 
# Build and run
```
npm i
npm run build
npm start
```
