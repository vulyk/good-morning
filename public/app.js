System.register("utils/misc", [], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    function isVisible(elt) {
        const style = window.getComputedStyle(elt);
        return (style.width !== null && +style.width !== 0)
            && (style.height !== null && +style.height !== 0)
            && (style.opacity !== null && +style.opacity !== 0)
            && style.display !== "none"
            && style.visibility !== "hidden";
    }
    exports_1("isVisible", isVisible);
    function tap(x, ...fns) {
        for (const fn of fns) {
            fn(x);
        }
        return x;
    }
    exports_1("tap", tap);
    function getRandomElement(array, getRandomFloat = Math.random) {
        return array[Math.floor(getRandomFloat() * array.length)];
    }
    exports_1("getRandomElement", getRandomElement);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("init/globals", [], function (exports_2, context_2) {
    "use strict";
    var width, height, syms, worldSide, playerStartX, playerStartY, tileSide, patternHalfSide, patternSide, patternHistoryWeight, patternHistoryWeights, patternHistoryDepth, palette;
    var __moduleName = context_2 && context_2.id;
    return {
        setters: [],
        execute: function () {
            exports_2("width", width = 1960);
            exports_2("height", height = 1080);
            exports_2("syms", syms = ["⋅", "🏵", "▓", "☘", "⸙"]);
            exports_2("worldSide", worldSide = 150);
            exports_2("playerStartX", playerStartX = Math.floor(worldSide / 2));
            exports_2("playerStartY", playerStartY = Math.floor(worldSide / 2));
            exports_2("tileSide", tileSide = 35);
            exports_2("patternHalfSide", patternHalfSide = 7);
            exports_2("patternSide", patternSide = patternHalfSide * 2 + 1);
            exports_2("patternHistoryWeight", patternHistoryWeight = 0.9);
            exports_2("patternHistoryWeights", patternHistoryWeights = [
                (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 0),
                (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 1),
                (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 2),
                (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 3),
                (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 4),
                (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 5),
                (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 6),
                (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 7),
                (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 8),
                (1 - patternHistoryWeight) * Math.pow(patternHistoryWeight, 9),
            ]);
            exports_2("patternHistoryDepth", patternHistoryDepth = patternHistoryWeights.length);
            exports_2("palette", palette = {
                back: 0xd0d0c0,
                main: 0x5050a0,
                attention: 0x101060,
                text: 0xd0d0d0,
            });
        }
    };
});
System.register("init/app", ["utils/misc", "pixi.js", "init/globals"], function (exports_3, context_3) {
    "use strict";
    var misc_1, pixi_js_1, globals, canvas, app;
    var __moduleName = context_3 && context_3.id;
    return {
        setters: [
            function (misc_1_1) {
                misc_1 = misc_1_1;
            },
            function (pixi_js_1_1) {
                pixi_js_1 = pixi_js_1_1;
            },
            function (globals_1) {
                globals = globals_1;
            }
        ],
        execute: function () {
            exports_3("canvas", canvas = document.getElementById("canvas"));
            exports_3("app", app = misc_1.tap(new pixi_js_1.Application({
                view: canvas,
                width: globals.width,
                height: globals.height,
                backgroundColor: globals.palette.back,
            }), app => {
                app.loader.baseUrl = "./assets/";
            }, app => {
                app.stage.position.set(app.renderer.width / 2, app.renderer.height / 2);
            }));
        }
    };
});
System.register("main", ["init/app"], function (exports_4, context_4) {
    "use strict";
    var app_1;
    var __moduleName = context_4 && context_4.id;
    return {
        setters: [
            function (app_1_1) {
                app_1 = app_1_1;
            }
        ],
        execute: function () {
            app_1.app.loader.load(() => {
                // noinspection JSIgnoredPromiseFromCall
                SystemJS.import("./run/run");
            });
        }
    };
});
System.register("init/layers", ["init/app", "pixi.js"], function (exports_5, context_5) {
    "use strict";
    var app_2, pixi_js_2, background, world, grail, fixedPattern, player, ui, win;
    var __moduleName = context_5 && context_5.id;
    return {
        setters: [
            function (app_2_1) {
                app_2 = app_2_1;
            },
            function (pixi_js_2_1) {
                pixi_js_2 = pixi_js_2_1;
            }
        ],
        execute: function () {
            exports_5("background", background = app_2.app.stage.addChild(new pixi_js_2.Container()));
            exports_5("world", world = app_2.app.stage.addChild(new pixi_js_2.Container()));
            exports_5("grail", grail = app_2.app.stage.addChild(new pixi_js_2.Container()));
            exports_5("fixedPattern", fixedPattern = app_2.app.stage.addChild(new pixi_js_2.Container()));
            exports_5("player", player = app_2.app.stage.addChild(new pixi_js_2.Container()));
            exports_5("ui", ui = app_2.app.stage.addChild(new pixi_js_2.Container()));
            exports_5("win", win = app_2.app.stage.addChild(new pixi_js_2.Container()));
        }
    };
});
System.register("init/textures", ["pixi.js", "init/app", "init/globals"], function (exports_6, context_6) {
    "use strict";
    var pixi_js_3, app_3, globals, syms, grail, grailHelp;
    var __moduleName = context_6 && context_6.id;
    function renderSym(sym) {
        const t = new pixi_js_3.Text();
        t.text = sym;
        t.style.fill = "white";
        t.style.fontSize = globals.tileSide;
        t.style.fontFamily = "Courier New";
        return app_3.app.renderer.generateTexture(t);
    }
    return {
        setters: [
            function (pixi_js_3_1) {
                pixi_js_3 = pixi_js_3_1;
            },
            function (app_3_1) {
                app_3 = app_3_1;
            },
            function (globals_2) {
                globals = globals_2;
            }
        ],
        execute: function () {
            exports_6("syms", syms = new Map([...globals.syms]
                .map(sym => [sym, renderSym(sym)])));
            exports_6("grail", grail = (() => {
                const t = new pixi_js_3.Text();
                t.text = "⚿";
                t.style.fontSize = globals.tileSide * 2;
                t.style.fontFamily = "Courier New";
                t.style.fill = globals.palette.attention;
                t.style.strokeThickness = 4;
                t.style.stroke = globals.palette.back;
                return app_3.app.renderer.generateTexture(t);
            })());
            exports_6("grailHelp", grailHelp = (() => {
                const t = new pixi_js_3.Text();
                t.text = "⭒";
                t.style.fontSize = globals.tileSide * 3;
                t.style.fontFamily = "Courier New";
                t.style.fill = globals.palette.attention;
                t.style.strokeThickness = 3;
                t.style.stroke = globals.palette.back;
                return app_3.app.renderer.generateTexture(t);
            })());
        }
    };
});
System.register("utils/CenteredSprite", ["pixi.js"], function (exports_7, context_7) {
    "use strict";
    var PIXI, Sprite, CenteredSprite;
    var __moduleName = context_7 && context_7.id;
    return {
        setters: [
            function (PIXI_1) {
                PIXI = PIXI_1;
            }
        ],
        execute: function () {
            Sprite = PIXI.Sprite;
            CenteredSprite = class CenteredSprite extends Sprite {
                constructor(texture) {
                    super(texture);
                    this.anchor.set(0.5);
                }
            };
            exports_7("CenteredSprite", CenteredSprite);
        }
    };
});
System.register("run/world", ["init/globals", "utils/misc", "pixi.js", "utils/CenteredSprite", "init/textures", "init/layers"], function (exports_8, context_8) {
    "use strict";
    var globals_3, misc_2, globals, pixi_js_4, CenteredSprite_1, textures, layers, WorldView, World;
    var __moduleName = context_8 && context_8.id;
    function generateWorld() {
        const world = Array.from({ length: globals_3.worldSide }, () => Array.from({ length: globals_3.worldSide }, () => globals.syms[0]));
        for (let i = 0; i < 20; i++) {
            const patchX = Math.floor(Math.random() * globals_3.worldSide);
            const patchY = Math.floor(Math.random() * globals_3.worldSide);
            const patchR = 20 + Math.round(Math.random() * 10);
            const sym = misc_2.getRandomElement(globals.syms);
            for (let dx = -patchR; dx <= patchR; dx++) {
                for (let dy = -patchR; dy <= patchR; dy++) {
                    if (dx * dx + dy * dy > patchR * patchR) {
                        continue;
                    }
                    const x = patchX + dx;
                    const y = patchY + dy;
                    if (x < 0 || x >= globals_3.worldSide || y < 0 || y >= globals_3.worldSide) {
                        continue;
                    }
                    world[x][y] = sym;
                }
            }
        }
        for (let i = 0; i < 1000; i++) {
            const patchX = Math.floor(Math.random() * globals_3.worldSide);
            const patchY = Math.floor(Math.random() * globals_3.worldSide);
            const patchR = 1 + Math.round(Math.random() * 2);
            const sym = misc_2.getRandomElement(globals.syms);
            for (let dx = -patchR; dx <= patchR; dx++) {
                for (let dy = -patchR; dy <= patchR; dy++) {
                    if (dx * dx + dy * dy > patchR * patchR) {
                        continue;
                    }
                    const x = patchX + dx;
                    const y = patchY + dy;
                    if (x < 0 || x >= globals_3.worldSide || y < 0 || y >= globals_3.worldSide) {
                        continue;
                    }
                    world[x][y] = sym;
                }
            }
        }
        for (let i = 0; i < 3000; i++) {
            const patchX = Math.floor(Math.random() * globals_3.worldSide);
            const patchY = Math.floor(Math.random() * globals_3.worldSide);
            const patchR = 0;
            const sym = misc_2.getRandomElement(globals.syms);
            for (let dx = -patchR; dx <= patchR; dx++) {
                for (let dy = -patchR; dy <= patchR; dy++) {
                    if (dx * dx + dy * dy > patchR * patchR) {
                        continue;
                    }
                    const x = patchX + dx;
                    const y = patchY + dy;
                    if (x < 0 || x >= globals_3.worldSide || y < 0 || y >= globals_3.worldSide) {
                        continue;
                    }
                    world[x][y] = sym;
                }
            }
        }
        return world;
    }
    return {
        setters: [
            function (globals_3_1) {
                globals_3 = globals_3_1;
                globals = globals_3_1;
            },
            function (misc_2_1) {
                misc_2 = misc_2_1;
            },
            function (pixi_js_4_1) {
                pixi_js_4 = pixi_js_4_1;
            },
            function (CenteredSprite_1_1) {
                CenteredSprite_1 = CenteredSprite_1_1;
            },
            function (textures_1) {
                textures = textures_1;
            },
            function (layers_1) {
                layers = layers_1;
            }
        ],
        execute: function () {
            WorldView = class WorldView extends pixi_js_4.Container {
                constructor(model) {
                    super();
                    this.model = model;
                    for (let x = 0; x < this.model.length; x++) {
                        for (let y = 0; y < this.model[x].length; y++) {
                            const tile = new CenteredSprite_1.CenteredSprite(textures.syms.get(this.model[x][y]));
                            tile.tint = globals.palette.main;
                            this.addChild(tile);
                            tile.position.set(x * globals_3.tileSide, y * globals_3.tileSide);
                        }
                    }
                    layers.world.addChild(this);
                }
            };
            World = class World {
                constructor() {
                    this.model = generateWorld();
                    this.view = new WorldView(this.model);
                }
            };
            exports_8("World", World);
        }
    };
});
System.register("run/pattern", ["run/modules", "pixi.js", "utils/CenteredSprite", "init/globals", "init/textures", "init/layers"], function (exports_9, context_9) {
    "use strict";
    var modules_1, pixi_js_5, CenteredSprite_2, globals_4, textures, globals, layers, FixedPatternView, Pattern;
    var __moduleName = context_9 && context_9.id;
    function copyPattern(cx, cy) {
        const fixedPattern = Array.from({ length: globals.patternSide }, () => Array.from({ length: globals.patternSide }));
        for (let x = -globals.patternHalfSide; x <= globals.patternHalfSide; x++) {
            for (let y = -globals.patternHalfSide; y <= globals.patternHalfSide; y++) {
                const wx = cx + x;
                const wy = cy + y;
                if (wx < 0 || wx >= globals.worldSide || wy < 0 || wy >= globals.worldSide) {
                    continue;
                }
                fixedPattern[globals.patternHalfSide + x][globals.patternHalfSide + y]
                    = modules_1.world.model[wx][wy];
            }
        }
        return fixedPattern;
    }
    function compareSyms(s1, s2) {
        if (s1 === s2) {
            return 1;
        }
        else {
            return 0;
        }
    }
    function comparePatterns(pattern, cx, cy) {
        let sum = 0;
        for (let x = -globals.patternHalfSide; x <= globals.patternHalfSide; x++) {
            for (let y = -globals.patternHalfSide; y <= globals.patternHalfSide; y++) {
                const wx = cx + x;
                const wy = cy + y;
                if (wx < 0 || wx >= globals.worldSide || wy < 0 || wy >= globals.worldSide) {
                    continue;
                }
                sum += compareSyms(pattern[globals.patternHalfSide + x][globals.patternHalfSide + y], modules_1.world.model[wx][wy]) * Math.min(1, 4 * Math.exp(-(x * x + y * y) / 4));
            }
        }
        return sum;
    }
    exports_9("comparePatterns", comparePatterns);
    function getProjection(model, cx, cy) {
        let sum = 0;
        for (let i = 0; i < globals.patternHistoryDepth; i++) {
            sum += globals.patternHistoryWeights[i] * comparePatterns(model.patternHistory[i], cx, cy);
        }
        return sum;
    }
    exports_9("getProjection", getProjection);
    return {
        setters: [
            function (modules_1_1) {
                modules_1 = modules_1_1;
            },
            function (pixi_js_5_1) {
                pixi_js_5 = pixi_js_5_1;
            },
            function (CenteredSprite_2_1) {
                CenteredSprite_2 = CenteredSprite_2_1;
            },
            function (globals_4_1) {
                globals_4 = globals_4_1;
                globals = globals_4_1;
            },
            function (textures_2) {
                textures = textures_2;
            },
            function (layers_2) {
                layers = layers_2;
            }
        ],
        execute: function () {
            FixedPatternView = class FixedPatternView extends pixi_js_5.Container {
                constructor(model) {
                    super();
                    this.model = model;
                    this.tiles = Array.from({ length: globals.patternSide }, () => Array.from({ length: globals.patternSide }));
                    for (let x = 0; x < globals.patternSide; x++) {
                        for (let y = 0; y < globals.patternSide; y++) {
                            const tile = this.tiles[x][y] = new CenteredSprite_2.CenteredSprite();
                            tile.tint = 0xf00000;
                            const _x = x - globals.patternHalfSide;
                            const _y = y - globals.patternHalfSide;
                            tile.alpha = 0.8 * Math.min(1, 4 * Math.exp(-(_x * _x + _y * _y) / 4));
                            this.addChild(tile);
                            tile.position.set(x * globals_4.tileSide, y * globals_4.tileSide);
                        }
                    }
                    this.position.set(-globals.patternHalfSide * globals_4.tileSide, -globals.patternHalfSide * globals_4.tileSide);
                    this.visible = false;
                    layers.fixedPattern.addChild(this);
                }
                updateTransform() {
                    for (let x = 0; x < globals.patternSide; x++) {
                        for (let y = 0; y < globals.patternSide; y++) {
                            const tile = this.tiles[x][y];
                            const w = {};
                            let maxW = 0;
                            let maxSym = globals.syms[0];
                            for (let i = 0; i < globals.patternHistoryDepth; i++) {
                                const sym = this.model.patternHistory[i][x][y];
                                if ("undefined" === typeof w[sym]) {
                                    w[sym] = 0;
                                }
                                w[sym] += globals.patternHistoryWeights[i];
                                if (w[sym] > maxW) {
                                    maxW = w[sym];
                                    maxSym = sym;
                                }
                            }
                            tile.texture = textures.syms.get(maxSym);
                        }
                    }
                    super.updateTransform();
                }
            };
            Pattern = class Pattern {
                constructor() {
                    const patternSlice = copyPattern(globals.playerStartX, globals.playerStartY);
                    this.model = {
                        patternHistory: Array.from({ length: globals.patternHistoryDepth }, () => patternSlice),
                    };
                    this.view = new FixedPatternView(this.model);
                    window.addEventListener("keydown", e => {
                        switch (e.keyCode) {
                            case 67: {
                                this.addSlice();
                                break;
                            }
                            case 86: {
                                this.view.visible = !this.view.visible;
                                break;
                            }
                        }
                    });
                }
                addSlice() {
                    this.model.patternHistory.pop();
                    this.model.patternHistory.unshift(copyPattern(modules_1.player.model.x, modules_1.player.model.y));
                }
            };
            exports_9("Pattern", Pattern);
        }
    };
});
System.register("run/player", ["pixi.js", "init/layers", "init/globals", "run/pattern", "run/modules"], function (exports_10, context_10) {
    "use strict";
    var pixi_js_6, layers, globals, pattern_1, modules_2, Direction, PlayerView, Player;
    var __moduleName = context_10 && context_10.id;
    function getSymForDirection(direction) {
        switch (direction) {
            case Direction.West: {
                return "⮜";
            }
            case Direction.East: {
                return "⮞";
            }
            case Direction.North: {
                return "⮝";
            }
            case Direction.South: {
                return "⮟";
            }
        }
    }
    return {
        setters: [
            function (pixi_js_6_1) {
                pixi_js_6 = pixi_js_6_1;
            },
            function (layers_3) {
                layers = layers_3;
            },
            function (globals_5) {
                globals = globals_5;
            },
            function (pattern_1_1) {
                pattern_1 = pattern_1_1;
            },
            function (modules_2_1) {
                modules_2 = modules_2_1;
            }
        ],
        execute: function () {
            (function (Direction) {
                Direction[Direction["North"] = 0] = "North";
                Direction[Direction["South"] = 1] = "South";
                Direction[Direction["East"] = 2] = "East";
                Direction[Direction["West"] = 3] = "West";
            })(Direction || (Direction = {}));
            exports_10("Direction", Direction);
            PlayerView = class PlayerView extends pixi_js_6.Text {
                constructor(model) {
                    super();
                    this.model = model;
                    this.style.fill = globals.palette.attention;
                    this.style.fontSize = globals.tileSide * 2;
                    this.style.fontFamily = "Courier New";
                    this.style.align = "center";
                    this.style.strokeThickness = 4;
                    this.style.stroke = globals.palette.back;
                    this.anchor.set(0.5);
                    layers.player.addChild(this);
                }
                updateTransform() {
                    this.text = getSymForDirection(this.model.direction);
                    layers.world.position.set(-this.model.x * globals.tileSide, -this.model.y * globals.tileSide);
                    layers.grail.position.set(-this.model.x * globals.tileSide, -this.model.y * globals.tileSide);
                    super.updateTransform();
                }
            };
            Player = class Player {
                constructor() {
                    this.model = {
                        x: globals.playerStartX,
                        y: globals.playerStartY,
                        direction: Direction.South,
                        discomfort: 0,
                        discomfortCap: 40,
                        expectedRest: 0,
                        day: 0,
                    };
                    this.updateExpectedRest();
                    this.view = new PlayerView(this.model);
                    window.addEventListener("keydown", e => {
                        if (modules_2.gameOverGui.gameOver) {
                            return;
                        }
                        switch (e.keyCode) {
                            case 65: {
                                this.move(Direction.West);
                                break;
                            }
                            case 68: {
                                this.move(Direction.East);
                                break;
                            }
                            case 87: {
                                this.move(Direction.North);
                                break;
                            }
                            case 83: {
                                this.move(Direction.South);
                                break;
                            }
                            case 32: {
                                this.sleep();
                                modules_2.pattern.addSlice();
                                this.updateExpectedRest();
                                break;
                            }
                        }
                    });
                }
                updateExpectedRest() {
                    this.model.expectedRest = Math.floor(pattern_1.getProjection(modules_2.pattern.model, this.model.x, this.model.y));
                }
                move(direction) {
                    switch (direction) {
                        case Direction.West: {
                            this.model.x--;
                            break;
                        }
                        case Direction.East: {
                            this.model.x++;
                            break;
                        }
                        case Direction.North: {
                            this.model.y--;
                            break;
                        }
                        case Direction.South: {
                            this.model.y++;
                            break;
                        }
                    }
                    this.model.direction = direction;
                    this.model.discomfort++;
                    this.updateExpectedRest();
                    if (this.model.discomfort > this.model.discomfortCap) {
                        modules_2.gameOverGui.lose();
                    }
                    if (this.model.x === modules_2.grail.model.x && this.model.y === modules_2.grail.model.y) {
                        modules_2.gameOverGui.win();
                    }
                }
                sleep() {
                    this.model.discomfort = Math.max(0, this.model.discomfort - this.model.expectedRest);
                    this.model.day++;
                }
            };
            exports_10("Player", Player);
        }
    };
});
System.register("run/discomfortGui", ["pixi.js", "init/layers", "run/modules", "init/globals"], function (exports_11, context_11) {
    "use strict";
    var pixi_js_7, layers, modules_3, globals, DiscomfortView, DiscomfortGui;
    var __moduleName = context_11 && context_11.id;
    return {
        setters: [
            function (pixi_js_7_1) {
                pixi_js_7 = pixi_js_7_1;
            },
            function (layers_4) {
                layers = layers_4;
            },
            function (modules_3_1) {
                modules_3 = modules_3_1;
            },
            function (globals_6) {
                globals = globals_6;
            }
        ],
        execute: function () {
            DiscomfortView = class DiscomfortView extends pixi_js_7.Container {
                constructor() {
                    super();
                    layers.ui.addChild(this);
                    this.text = new pixi_js_7.Text();
                    this.text.style.fill = globals.palette.text;
                    this.text.style.fontSize = 70;
                    this.text.style.strokeThickness = 8;
                    this.text.style.stroke = globals.palette.attention;
                    this.addChild(this.text);
                    this.position.set(-globals.width / 2 + 30, -globals.height / 2 + 30);
                }
                updateTransform() {
                    this.text.text = ` (>_<)   ${modules_3.player.model.discomfort} (-${modules_3.player.model.expectedRest})`
                        + ` / ${modules_3.player.model.discomfortCap}`;
                    super.updateTransform();
                }
            };
            DiscomfortGui = class DiscomfortGui {
                constructor() {
                    this.view = new DiscomfortView();
                }
            };
            exports_11("DiscomfortGui", DiscomfortGui);
        }
    };
});
System.register("run/dayGui", ["pixi.js", "init/layers", "run/modules", "init/globals"], function (exports_12, context_12) {
    "use strict";
    var pixi_js_8, layers, modules_4, globals, DayView, DayGui;
    var __moduleName = context_12 && context_12.id;
    return {
        setters: [
            function (pixi_js_8_1) {
                pixi_js_8 = pixi_js_8_1;
            },
            function (layers_5) {
                layers = layers_5;
            },
            function (modules_4_1) {
                modules_4 = modules_4_1;
            },
            function (globals_7) {
                globals = globals_7;
            }
        ],
        execute: function () {
            DayView = class DayView extends pixi_js_8.Container {
                constructor() {
                    super();
                    layers.ui.addChild(this);
                    this.text = new pixi_js_8.Text();
                    this.text.style.fill = globals.palette.text;
                    this.text.style.fontSize = 70;
                    this.text.style.strokeThickness = 8;
                    this.text.style.stroke = globals.palette.attention;
                    this.addChild(this.text);
                    this.position.set(-globals.width / 2 + 30, -globals.height / 2 + 30 + this.text.height + 10);
                }
                updateTransform() {
                    this.text.text = `    🛏     ${modules_4.player.model.day} `;
                    super.updateTransform();
                }
            };
            DayGui = class DayGui {
                constructor() {
                    this.view = new DayView();
                }
            };
            exports_12("DayGui", DayGui);
        }
    };
});
System.register("run/grail", ["init/globals", "pixi.js", "utils/CenteredSprite", "init/textures", "init/layers"], function (exports_13, context_13) {
    "use strict";
    var globals, pixi_js_9, CenteredSprite_3, textures, layers, GrailView, Grail;
    var __moduleName = context_13 && context_13.id;
    function createGrail() {
        const a = Math.random() * Math.PI * 2;
        const helpA = a + (Math.random() - 0.5);
        const r = globals.worldSide / 2 * 0.7;
        return {
            x: globals.playerStartX + Math.round(r * Math.sin(a)),
            y: globals.playerStartY + Math.round(r * Math.cos(a)),
            helpX: globals.playerStartX + Math.round(r * 0.4 * Math.sin(helpA)),
            helpY: globals.playerStartY + Math.round(r * 0.4 * Math.cos(helpA)),
        };
    }
    return {
        setters: [
            function (globals_8) {
                globals = globals_8;
            },
            function (pixi_js_9_1) {
                pixi_js_9 = pixi_js_9_1;
            },
            function (CenteredSprite_3_1) {
                CenteredSprite_3 = CenteredSprite_3_1;
            },
            function (textures_3) {
                textures = textures_3;
            },
            function (layers_6) {
                layers = layers_6;
            }
        ],
        execute: function () {
            GrailView = class GrailView extends pixi_js_9.Container {
                constructor(model) {
                    super();
                    this.model = model;
                    const tile = new CenteredSprite_3.CenteredSprite(textures.grail);
                    this.addChild(tile);
                    tile.position.set(this.model.x * globals.tileSide, this.model.y * globals.tileSide);
                    const helpTile = new CenteredSprite_3.CenteredSprite(textures.grailHelp);
                    this.addChild(helpTile);
                    helpTile.position.set(this.model.helpX * globals.tileSide, this.model.helpY * globals.tileSide);
                    layers.grail.addChild(this);
                }
            };
            Grail = class Grail {
                constructor() {
                    this.model = createGrail();
                    this.view = new GrailView(this.model);
                }
            };
            exports_13("Grail", Grail);
        }
    };
});
System.register("run/gameOverGui", ["pixi.js", "init/layers", "run/modules", "init/globals"], function (exports_14, context_14) {
    "use strict";
    var pixi_js_10, layers, modules_5, globals, GameOverView, GameOverGui;
    var __moduleName = context_14 && context_14.id;
    return {
        setters: [
            function (pixi_js_10_1) {
                pixi_js_10 = pixi_js_10_1;
            },
            function (layers_7) {
                layers = layers_7;
            },
            function (modules_5_1) {
                modules_5 = modules_5_1;
            },
            function (globals_9) {
                globals = globals_9;
            }
        ],
        execute: function () {
            GameOverView = class GameOverView extends pixi_js_10.Container {
                constructor() {
                    super();
                    layers.ui.addChild(this);
                    this.text = new pixi_js_10.Text();
                    this.text.style.fill = globals.palette.text;
                    this.text.style.strokeThickness = 25;
                    this.text.style.stroke = globals.palette.attention;
                    this.text.anchor.set(0.5);
                    this.starText = new pixi_js_10.Text();
                    this.starText.style.fill = globals.palette.attention;
                    this.starText.style.fontSize = 450;
                    this.starText.style.strokeThickness = 30;
                    this.starText.style.stroke = globals.palette.text;
                    this.starText.anchor.set(0.5);
                    this.addChild(this.text);
                    this.addChild(this.starText);
                    this.visible = false;
                }
                win(stars) {
                    this.text.text = `＼( ^⋅^ )／`;
                    this.text.style.fontSize = 150;
                    this.text.position.set(0, -150);
                    let starsText = "";
                    for (let i = 0; i < 5; i++) {
                        starsText += i < stars ? "⭒" : "⭑";
                    }
                    this.starText.text = starsText;
                    this.starText.position.set(0, 150);
                    this.visible = true;
                }
                lose() {
                    this.text.text = `☠`;
                    this.text.style.fontSize = 550;
                    this.text.position.set(0, 0);
                    this.starText.text = "";
                    this.visible = true;
                }
            };
            GameOverGui = class GameOverGui {
                constructor() {
                    this.gameOver = false;
                    this.view = new GameOverView();
                }
                win() {
                    if (this.gameOver) {
                        return;
                    }
                    this.gameOver = true;
                    this.view.win((() => {
                        if (modules_5.player.model.day < 5) {
                            return 5;
                        }
                        else if (modules_5.player.model.day < 10) {
                            return 4;
                        }
                        else if (modules_5.player.model.day < 20) {
                            return 3;
                        }
                        else if (modules_5.player.model.day < 50) {
                            return 2;
                        }
                        else if (modules_5.player.model.day < 100) {
                            return 1;
                        }
                        else {
                            return 0;
                        }
                    })());
                }
                lose() {
                    if (this.gameOver) {
                        return;
                    }
                    this.gameOver = true;
                    this.view.lose();
                }
            };
            exports_14("GameOverGui", GameOverGui);
        }
    };
});
System.register("run/modules", ["run/world", "run/player", "run/pattern", "run/discomfortGui", "run/dayGui", "run/grail", "run/gameOverGui", "run/controlsGui"], function (exports_15, context_15) {
    "use strict";
    var world_1, player_1, pattern_2, discomfortGui_1, dayGui_1, grail_1, gameOverGui_1, controlsGui_1, world, grail, pattern, player, discomfortGui, dayGui, gameOverGui, controlsGui;
    var __moduleName = context_15 && context_15.id;
    return {
        setters: [
            function (world_1_1) {
                world_1 = world_1_1;
            },
            function (player_1_1) {
                player_1 = player_1_1;
            },
            function (pattern_2_1) {
                pattern_2 = pattern_2_1;
            },
            function (discomfortGui_1_1) {
                discomfortGui_1 = discomfortGui_1_1;
            },
            function (dayGui_1_1) {
                dayGui_1 = dayGui_1_1;
            },
            function (grail_1_1) {
                grail_1 = grail_1_1;
            },
            function (gameOverGui_1_1) {
                gameOverGui_1 = gameOverGui_1_1;
            },
            function (controlsGui_1_1) {
                controlsGui_1 = controlsGui_1_1;
            }
        ],
        execute: function () {
            exports_15("world", world = new world_1.World());
            exports_15("grail", grail = new grail_1.Grail());
            exports_15("pattern", pattern = new pattern_2.Pattern());
            exports_15("player", player = new player_1.Player());
            exports_15("discomfortGui", discomfortGui = new discomfortGui_1.DiscomfortGui());
            exports_15("dayGui", dayGui = new dayGui_1.DayGui());
            exports_15("gameOverGui", gameOverGui = new gameOverGui_1.GameOverGui());
            exports_15("controlsGui", controlsGui = new controlsGui_1.ControlsGui());
        }
    };
});
System.register("run/controlsGui", ["pixi.js", "init/layers", "init/globals"], function (exports_16, context_16) {
    "use strict";
    var pixi_js_11, layers, globals, ControlsView, ControlsGui;
    var __moduleName = context_16 && context_16.id;
    return {
        setters: [
            function (pixi_js_11_1) {
                pixi_js_11 = pixi_js_11_1;
            },
            function (layers_8) {
                layers = layers_8;
            },
            function (globals_10) {
                globals = globals_10;
            }
        ],
        execute: function () {
            ControlsView = class ControlsView extends pixi_js_11.Container {
                constructor() {
                    super();
                    layers.ui.addChild(this);
                    this.text = new pixi_js_11.Text();
                    this.text.text = " |W|\n|A||S||D| |____|\n";
                    this.text.style.fill = globals.palette.text;
                    this.text.style.fontFamily = "Courier New";
                    this.text.style.fontSize = 50;
                    this.text.style.strokeThickness = 8;
                    this.text.style.stroke = globals.palette.attention;
                    this.text.anchor.set(0, 1);
                    this.addChild(this.text);
                    this.position.set(-globals.width / 2 + 30, +globals.height / 2 + 30);
                }
                updateTransform() {
                    super.updateTransform();
                }
            };
            ControlsGui = class ControlsGui {
                constructor() {
                    this.view = new ControlsView();
                }
            };
            exports_16("ControlsGui", ControlsGui);
        }
    };
});
System.register("run/run", ["run/modules"], function (exports_17, context_17) {
    "use strict";
    var __moduleName = context_17 && context_17.id;
    return {
        setters: [
            function (_1) {
            }
        ],
        execute: function () {
            window.addEventListener("keydown", e => {
                // console.log(e.keyCode);
                switch (e.keyCode) {
                }
            });
        }
    };
});
// https://en.wikipedia.org/wiki/Lehmer_random_number_generator
System.register("utils/Random", [], function (exports_18, context_18) {
    "use strict";
    var MAX_INT32, MINSTD, Random;
    var __moduleName = context_18 && context_18.id;
    return {
        setters: [],
        execute: function () {
            MAX_INT32 = 2147483647;
            MINSTD = 16807;
            Random = class Random {
                constructor(seed) {
                    if (!Number.isInteger(seed)) {
                        throw new TypeError("Expected `seed` to be a `integer`");
                    }
                    this.seed = seed % MAX_INT32;
                    if (this.seed <= 0) {
                        this.seed += (MAX_INT32 - 1);
                    }
                }
                next() {
                    return this.seed = this.seed * MINSTD % MAX_INT32;
                }
                nextFloat() {
                    return (this.next() - 1) / (MAX_INT32 - 1);
                }
            };
            exports_18("Random", Random);
        }
    };
});
//# sourceMappingURL=app.js.map